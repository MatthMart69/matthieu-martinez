#include "feutre.h"
#include <iostream>

const std::string reset("\033[0m");
const std::string red("\033[0;31m");
const std::string green("\033[0;32m");
const std::string yellow("\033[0;33m");
const std::string blue("\033[0;34m");
const std::string magenta("\033[0;35m");
const std::string cyan("\033[0;36m");

Feutre::Feutre(const std::string& couleur) : couleur(couleur), debouche(true) {}

Feutre::~Feutre() {}

std::string Feutre::getCouleur() const {
    return couleur;
}

bool Feutre::estBouche() const {
    return debouche;
}

void Feutre::EcrireMessage(const std::string& message) {
    if (debouche) {
        std::string couleurCode;
        if (couleur == "rouge") {
            couleurCode = red;
        }
        else if (couleur == "vert") {
            couleurCode = green;
        }
        else if (couleur == "jaune") {
            couleurCode = yellow;
        }
        else if (couleur == "bleu") {
            couleurCode = blue;
        }
        else if (couleur == "magenta") {
            couleurCode = magenta;
        }
        else if (couleur == "cyan") {
            couleurCode = cyan;
        }
        else {
            couleurCode = reset;
        }

        std::cout << couleurCode << "Ecrit avec un feutre de couleur " << couleur << " : " << message << reset << std::endl;
    }
    else {
        std::cout << "Le feutre est bouche, vous ne pouvez pas ecrire." << std::endl;
    }
}

void Feutre::Bouche() {
    debouche = false;
}

void Feutre::Debouche() {
    debouche = true;
}
