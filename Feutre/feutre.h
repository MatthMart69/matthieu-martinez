#ifndef FEUTRE_H
#define FEUTRE_H

#include <string>

class Feutre {
public:
    Feutre(const std::string& couleur);
    ~Feutre();

    std::string getCouleur() const;
    bool estBouche() const;

    void EcrireMessage(const std::string& message);
    void Bouche();
    void Debouche();

private:
    std::string couleur;
    bool debouche;
};

#endif
