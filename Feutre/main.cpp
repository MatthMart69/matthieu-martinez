#include "feutre.h"
#include <vector>

int main() {
    std::vector<Feutre> feutres;
    feutres.push_back(Feutre("rouge"));
    feutres.push_back(Feutre("bleu"));
    feutres.push_back(Feutre("vert"));
    feutres.push_back(Feutre("jaune"));
    feutres.push_back(Feutre("magenta"));
    feutres.push_back(Feutre("cyan"));

    // �crivez des messages avec les feutres
    for (auto& feutre : feutres) {
        feutre.EcrireMessage("Ceci est un message avec le feutre de couleur " + feutre.getCouleur());
    }

    return 0;
}
